# DAWeC

Data Analysis Web Component (DAWeC) is a Web Application that allows an
user-friendly execution of the s3-scraper.

## Requirements

It has been tested on Ubuntu 19.10, but should work in any GNU/Linux with the utilities below:

* [Maven](https://maven.apache.org/) - Dependency Management

### Client side
* [Angular 8](https://angular.io/) - Web application framework

### Server side
* [Spring Boot](https://spring.io/) - Application framework
* [SQLite 3](https://www.sqlite.org) - RDBMS
* [Hibernate](http://hibernate.org/) - Object-relational mapping tool
* [Cron](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/crontab.html) - Job scheduler
* s3-scraper - Web Scraper


## Installation

1. Get the code from [GitLab](https://gitlab.com/rpizziol/dawec) or directly
clone it from the git repository:
```
git clone https://gitlab.com/rpizziol/dawec.git
```

2. Inside `/DAWeC/ServerDAWeC/s3scraperWrapper.py` change the path of the
s3scraper application according to your location:

```
child = pexpect.spawn('python3 /path/to/scraper/s3-scraper-master/s3scraper/scraper.py')
```

3. Generate the database file inside `/DAWeC/ServerDAWeC`:
```
sqlite3 s3scraper.db ';'
```

4. In order to allow concurrent access operations to the database, it is required
to enable the Write-Ahead Log option:
```
sqlite3 s3scraper.db 'PRAGMA journal_mode=WAL;'
```

## Usage

### Server side
* Run the Spring server application.
* Check if server is running at [localhost:8080](http://localhost:8080).

### Client side
* Run the Angular application (`ng serve`).
* Open the client application at [localhost:4200](http://localhost:4200). Login credentials are: {Username: user, Password: pwd}.


## Versioning
For the versions available, see the [tags on this repository](https://gitlab.com/rpizziol/dawec/-/tags). 

## Authors

* **Roberto Pizziol** - *DAWeC web application* - [rpizziol](https://gitlab.com/rpizziol/)
