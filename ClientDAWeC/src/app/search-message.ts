export class SearchMessage {
  constructor(
    public id: number,
    public inputText: string,
    public targetSite: string, // TODO make this an array of sites
    public searchLimit: number,
    public numberOfProxies: number,
    public details: boolean,
    public automaticMode: boolean,
    public frequency: string,
    public automaticTime: string,
    public timeout: number
  ) {
  }
}
