import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SearchMessage} from '../search-message';
import {Observable} from 'rxjs';
import {Feedback} from '../feedback';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};


@Injectable({
  providedIn: 'root'
})

export class HttpService {


  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get('http://localhost:8080/products');
  }

  getSearches() {
    return this.http.get('http://localhost:8080/history');
  }


  addSearchMessage(searchMessage: SearchMessage): Observable<SearchMessage> {
    return this.http.post<SearchMessage>('http://localhost:8080/search', searchMessage, httpOptions);
    // TODO: Handle error
  }

  addFeedbackMessage(feedback: Feedback): Observable<Feedback> {
    return this.http.post<Feedback>('http://localhost:8080/feedback', feedback, httpOptions);
    // TODO: Handle error
  }

}
