import {Component, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';
import {Feedback} from '../feedback';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {FeedbackDialogComponent} from '../feedback-dialog/feedback-dialog.component';

class FilterObject {
  constructor(
    public filterText: string,
    public historyFilter: string
  ) {
  }
}

@Component({
  selector: 'app-data',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  anyHistory = 'Any';
  model = new FilterObject('', this.anyHistory);
  products: any;
  filteredProducts: any;
  searches: any;
  searchesString: string[] = [];
  feedback: Feedback;

  constructor(private httpService: HttpService, private dialog: MatDialog) {
  }

  ngOnInit() {
    // Obtain product list
    this.httpService.getProducts().subscribe(data => {
      this.products = data;
      this.assignCopy();
    });

    // Fill up the drop down menu of searches
    this.httpService.getSearches().subscribe(data => {
      this.searches = data;
      this.searchesString[0] = this.anyHistory;
      for (let i = 0; i < this.searches.length; i++) {
        this.searchesString[i + 1] = JSON.stringify(this.searches[i]);
      }
    });
  }

  assignCopy() {
    // this.filteredProducts = Object.assign([], this.products);
    this.filterItemWithUniqueUrl();
  }

  // When the 'Filter' button is pressed
  onSubmit() {
    console.log(this.model);
    if (this.model.historyFilter === this.anyHistory) {
      if (this.model.filterText === '') {
        this.filterItemWithUniqueUrl();
      } else {
        this.filterItemByText(this.model.filterText);
      }
    } else {
      this.filterItemByHistory(this.model.filterText, JSON.parse(this.model.historyFilter).id);
    }
  }

  // Open the input dialog for feedback
  openDialog(productId, score, preferred, textNote) {
    const dialogConfig = new MatDialogConfig();
    console.log('preferred is ' + preferred);
    this.feedback = new Feedback(productId, score, preferred === 1, textNote);
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      current_feedback: this.feedback
    };
    this.dialog.open(FeedbackDialogComponent, dialogConfig);
  }

  // Get the list of all products, given they have different Url
  filterItemWithUniqueUrl() {
    this.filteredProducts = Object.assign([], this.products).filter(
      // tslint:disable-next-line:new-parens
      (types => item => !types.has(item.url) && types.add(item.url))(new Set));
  }

  // Get the list of products whose title has the 'value' word in it
  filterItemByText(value) {
    if (!value) {
      this.assignCopy();
    }
    this.filteredProducts = Object.assign([], this.products).filter(
      item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
    );
  }

  // Get the list of products whose title has the 'value' word in it and that has been obtained from the search 'history'
  filterItemByHistory(value, history) {
    if (!value) {
      this.assignCopy();
    }
    console.log(history);
    this.filteredProducts = Object.assign([], this.products).filter(
      item => (item.name.toLowerCase().indexOf(value.toLowerCase()) > -1 && item.searchId === history)
    );
  }
}
