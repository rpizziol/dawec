import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {SearchComponent} from './search/search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductsComponent} from './products/products.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {HistoryComponent} from './history/history.component';
import {FeedbackDialogComponent} from './feedback-dialog/feedback-dialog.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ProductsComponent,
    LoginComponent,
    LogoutComponent,
    HistoryComponent,
    FeedbackDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [FeedbackDialogComponent]
})
export class AppModule {
}
