import {Component, Inject, OnInit} from '@angular/core';
import {Feedback} from '../feedback';
import {HttpService} from '../service/http.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-feedback-dialog',
  templateUrl: './feedback-dialog.component.html',
  styleUrls: ['./feedback-dialog.component.scss']
})
export class FeedbackDialogComponent implements OnInit {

  private model: Feedback;
  feedbacks: Feedback[];
  public min = 0;
  public max = 10;
  public step = 0.1;
  private invalidScoreInput = false;

  constructor(private httpService: HttpService,
              private dialogRef: MatDialogRef<FeedbackDialogComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
    this.model = data.current_feedback;
  }

  ngOnInit() {
  }

  onSubmitFeedback() {
    console.log('productid = ' + this.model.productId);
    console.log('model = ' + JSON.stringify(this.model));
    this.httpService.addFeedbackMessage(this.model).subscribe(feedback => this.feedbacks.push(feedback));
    this.dialogRef.close();
  }

  onKeyUpScore(event) {
    if (event.currentTarget.value === '') {
      this.invalidScoreInput = true;
    } else {
      const scoreNumber = Number(event.currentTarget.value);
      this.invalidScoreInput = !(scoreNumber >= this.min && scoreNumber <= this.max);
    }
  }
}
