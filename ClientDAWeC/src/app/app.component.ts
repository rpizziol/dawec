import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './service/authentication.service';

// Component decorator (locations of important files)
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private loginService: AuthenticationService) {
  }

  ngOnInit(): void {
  }
}
