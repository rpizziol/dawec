import {Component, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  // tslint:disable-next-line:ban-types
  searches: Object;
  displayedColumns: string[] = ['id', 'searchParams', 'searchDate'];

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.getSearches().subscribe(data => {
      this.searches = data;
      console.log(this.searches);
    });
  }

}
