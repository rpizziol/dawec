import {Component, OnInit} from '@angular/core';
import {SearchMessage} from '../search-message';
import {HttpService} from '../service/http.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  messages: SearchMessage[];
  targetSites = ['Amazon']; // , 'ebay', 'AliExpress']; TODO add also ebay and AliExpress
  frequencies = ['hourly', 'daily', 'weekly'];
  model: SearchMessage;
  id: number; // 'id' counts the number of messages being sent in one "search session"
  submitted = false;

  // Values for input control
  public invalidProxiesInput = false;
  public invalidSearchLimitInput = false;
  public invalidTimeoutInput = false;
  public step = 1;
  public min = 1;
  public minSearchLimit = -1;

  constructor(private httpService: HttpService, private dialog: MatDialog) {
  }

  onSubmit(templateRef) {
    // Obtain different input phrases
    const inputPhrases = this.model.inputText.split('\n');
    for (const inputPhrase of inputPhrases) {
      // Increment search id
      this.id++;
      this.model.id = this.id;
      // Generate the message to be sent
      const newMessage = new SearchMessage(this.model.id, inputPhrase, this.model.targetSite, this.model.searchLimit,
        this.model.numberOfProxies, this.model.details, this.model.automaticMode, this.model.frequency, this.model.automaticTime,
        this.model.timeout);
      console.log(newMessage);
      // TODO fix ERROR TypeError: "this.messages is undefined"
      this.httpService.addSearchMessage(newMessage).subscribe(message => this.messages.push(message));
    }

    // Note: considered as submitted even if server is not running
    this.submitted = true;
    const dialogRef = this.dialog.open(templateRef, {});
  }

  ngOnInit() {
    this.id = 0;
    this.defaultSearchMessage(this.id);
  }

  defaultSearchMessage(id) {
    const now = new Date();
    const formattedNowTime = now.getHours().toLocaleString() + ':' + now.getMinutes().toLocaleString();
    this.model = new SearchMessage(id, '', this.targetSites[0], 10, 10, false,
      false, this.frequencies[0], formattedNowTime, 60);
    console.log(this.model);
  }

  // Functions for input control
  public onKeyUpProxies(event) {
    if (event.currentTarget.value === '') {
      this.invalidProxiesInput = true;
    } else {
      const proxiesNumber = Number(event.currentTarget.value);
      this.invalidProxiesInput = !((proxiesNumber >= this.min && proxiesNumber === parseInt(String(proxiesNumber), 10)));
    }
  }

  public onKeyUpTimeout(event) {
    if (event.currentTarget.value === '') {
      this.invalidTimeoutInput = true;
    } else {
      const timeoutNumber = Number(event.currentTarget.value);
      this.invalidTimeoutInput = !((timeoutNumber >= this.min && timeoutNumber === parseInt(String(timeoutNumber), 10)));
    }
  }

  public onKeyUpSearchLimit(event) {
    if (event.currentTarget.value === '') {
      this.invalidSearchLimitInput = true;
    } else {
      const searchLimitNumber = Number(event.currentTarget.value);
      this.invalidSearchLimitInput =
        !((searchLimitNumber >= this.minSearchLimit && searchLimitNumber === parseInt(String(searchLimitNumber), 10)))
        || 0 === searchLimitNumber;
    }
  }
}
