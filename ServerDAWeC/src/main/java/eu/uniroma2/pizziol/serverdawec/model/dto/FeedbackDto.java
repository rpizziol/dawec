package eu.uniroma2.pizziol.serverdawec.model.dto;

import java.io.Serializable;


/**
 * Data Transfer Object class used to store data sent by Client application during feedback.
 */
public class FeedbackDto implements Serializable {

    private int productId;
    private double score;
    private boolean preferred;
    private String textNote;

    public FeedbackDto(int productId, double score, boolean preferred, String textNote) {
        this.productId = productId;
        this.score = score;
        this.preferred = preferred;
        this.textNote = textNote;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public boolean isPreferred() {
        return preferred;
    }

    public void setPreferred(boolean preferred) {
        this.preferred = preferred;
    }

    public String getTextNote() {
        return textNote;
    }

    public void setTextNote(String textNote) {
        this.textNote = textNote;
    }
}
