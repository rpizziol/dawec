package eu.uniroma2.pizziol.serverdawec.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "search")
public class Search {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int id;

    @Column(name = "search_params")
    private String searchParams;

    @Column(name = "search_date")
    private Date searchDate;

    public Search() {
    }

    public Search(int id) {
        this.id = id;
    }

    public Search(int id, String searchParams, Date searchDate) {
        this.id = id;
        this.searchParams = searchParams;
        this.searchDate = searchDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(String searchParams) {
        this.searchParams = searchParams;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }
}
