package eu.uniroma2.pizziol.serverdawec;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory = initHibernateUtil();

    private static SessionFactory initHibernateUtil() {
        try {
            Configuration config = new Configuration();
            // The database will be newly created by the python script inside this application
            config.setProperty("hibernate.connection.url",
                    "jdbc:sqlite:/" + System.getProperty("user.dir") + "/s3scraper.db");
            return config.configure().buildSessionFactory();
        } catch (HibernateException ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
}
