package eu.uniroma2.pizziol.serverdawec.controller;

import eu.uniroma2.pizziol.serverdawec.HibernateUtil;
import eu.uniroma2.pizziol.serverdawec.model.Feedback;
import eu.uniroma2.pizziol.serverdawec.model.Product;
import eu.uniroma2.pizziol.serverdawec.model.Search;
import eu.uniroma2.pizziol.serverdawec.model.dto.FeedbackDto;
import eu.uniroma2.pizziol.serverdawec.model.dto.FullProductDto;
import eu.uniroma2.pizziol.serverdawec.model.dto.SearchMessageDto;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class RequestsController {

    @GetMapping("/products")
    public List<FullProductDto> showProducts() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        updateFeedbackTable();

        // Same result as two nested INNER JOINs
        String hql = "SELECT p.id, p.name, p.ASIN, " +
                "d.id, d.description, d.url, d.search.id, d.imageUrl, " +
                "f.score, f.preferred, f.textNote " +
                "FROM Product p, ProductDetail d, Feedback f " +
                "WHERE p.id = d.product AND p.id = f.product";
        Query q = session.createQuery(hql);

        List<FullProductDto> fullProductDtos = new LinkedList<>();
        for (Iterator it = q.iterate(); it.hasNext(); ) {
            Object[] row = (Object[]) it.next();
            // TODO add all content of ProductDetail
            FullProductDto fullProductDto = new FullProductDto(
                    (Integer) row[0],   // p.id
                    (String) row[1],    // p.name
                    (String) row[2],    // p.ASIN
                    (Integer) row[3],   // d.id
                    (String) row[4],    // d.description
                    (String) row[5],    // d.url
                    (Integer) row[6],   // d.search.id
                    (String) row[7],   // d.image_url
                    (Double) row[8],    // f.score
                    (Integer) row[9],   // f.preferred
                    (String) row[10]);   // f.textNote
            fullProductDtos.add(fullProductDto);
        }
        return fullProductDtos;
    }

    @GetMapping("/history")
    public List<Search> showHistory() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Search> searches = new LinkedList<>();
        Query query = session.createQuery("FROM Search");
        for (final Object o : query.list()) {
            searches.add((Search) o);
        }
        return searches;
    }

    @PostMapping("/feedback")
    public void postFeedback(@RequestBody FeedbackDto feedbackDto) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Product product = (Product) session.get(Product.class, feedbackDto.getProductId());
        Feedback feedback = new Feedback(product, feedbackDto.getScore(),
                (feedbackDto.isPreferred() ? 1 : 0), feedbackDto.getTextNote());
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(feedback);
        transaction.commit();
    }

    @PostMapping("/search")
    public void searchRequest(@RequestBody SearchMessageDto searchMessageDto) {
        try {
            System.out.println("The user is looking for \"" + searchMessageDto.getInputText() + "\".");
            String wrapperCommand = buildWrapperCommand(searchMessageDto);
            String command;
            if (searchMessageDto.isAutomaticMode()) {
                String username = System.getProperty("user.name");
                String cronTime = searchMessageDto.getAutomaticTime().substring(3);
                if (searchMessageDto.getFrequency().equals("hourly")) {
                    cronTime += " */1 * * * ";
                } else if (searchMessageDto.getFrequency().equals("daily")) {
                    cronTime += " " + searchMessageDto.getAutomaticTime().substring(0, 2) + " * * * ";
                } else { // weekly
                    cronTime += " " + searchMessageDto.getAutomaticTime().substring(0, 2) + " * * 0 ";
                }
                command = "{ crontab -u " + username + " -l; echo \"" + cronTime + "timeout " +
                        searchMessageDto.getTimeout() + " " + wrapperCommand + "\"; } " +
                        "| crontab -u " + username + " -";
            } else {
                command = wrapperCommand;
            }
            System.out.println(command);
            runCommand(command);
        } catch (IOException e) {
            System.out.println("Exception happened - here's what I know: ");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Given the required parameters, builds a String used to launch the wrapper of the s3scraper Python application.
     *
     * @param searchMessageDto The received message with required parameters.
     * @return The String correctly formatted in order to run the wrapper.
     */
    String buildWrapperCommand(SearchMessageDto searchMessageDto) {
        return System.getProperty("user.dir") + "/exec_scraper.sh " +
                (searchMessageDto.getTargetSite().equals("Amazon") ? "1 " : "2 ")
                + "1 " + // Code for search
                (searchMessageDto.isDetails() ? 'y' : 'n') + " " +
                searchMessageDto.getSearchLimit() + " " +
                searchMessageDto.getNumberOfProxies() + " " +
                searchMessageDto.getInputText();
    }


    /**
     * Execute a different process.
     *
     * @param command The command used to run the process in the prompt.
     * @throws IOException Exception.
     */
    public void runCommand(String command) throws IOException {
        String s;
        String[] cmd = {"/bin/sh", "-c", command};
        Process p = Runtime.getRuntime().exec(cmd);

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

        // Output from the command
        System.out.println("Here is the standard output of the command:\n");
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }

        // Errors from the attempted command
        System.out.println("Here is the standard error of the command (if any):\n");
        while ((s = stdError.readLine()) != null) {
            System.out.println(s);
        }

        // Kill the process
        p.destroy();
    }

    /**
     * Update the feedback table of the database with default values.
     */
    private void updateFeedbackTable() {
        // Populate the table with default values for products that do not have a feedback
        String updateSQLiteCommand = "'INSERT INTO feedback (product) " +
                "SELECT id " +
                "FROM product WHERE id NOT IN " +
                "(SELECT product " +
                "FROM feedback);'";

        // Prepare the two sqlite commands to execute
        String refreshFeedback = "sqlite3 " + System.getProperty("user.dir") + "/s3scraper.db " + updateSQLiteCommand;
        try {
            runCommand(refreshFeedback);
        } catch (IOException e) {
            System.out.println("Exception happened - here's what I know: ");
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
