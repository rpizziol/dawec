package eu.uniroma2.pizziol.serverdawec.model.dto;

public class FullProductDto {
    private int id;
    private String name;
    private String ASIN;
    private int productDetailId;
    private String description;
    private String url;
    private int searchId;
    private String imageUrl;

    // Feedback information
    private Double score;
    private Integer preferred;
    private String textNote;

    public FullProductDto(int id, String name, String ASIN, int productDetailId, String description, String url,
                          int searchId, String imageUrl, Double score, Integer preferred, String textNote) {
        this.id = id;
        this.name = name;
        this.ASIN = ASIN;
        this.productDetailId = productDetailId;
        this.description = description;
        this.url = url;
        this.searchId = searchId;
        this.imageUrl = imageUrl;
        this.score = score;
        this.preferred = preferred;
        this.textNote = textNote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String ASIN) {
        this.ASIN = ASIN;
    }

    public int getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(int productDetailId) {
        this.productDetailId = productDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSearchId() {
        return searchId;
    }

    public void setSearchId(int searchId) {
        this.searchId = searchId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getPreferred() {
        return preferred;
    }

    public void setPreferred(Integer preferred) {
        this.preferred = preferred;
    }

    public String getTextNote() {
        return textNote;
    }

    public void setTextNote(String textNote) {
        this.textNote = textNote;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
