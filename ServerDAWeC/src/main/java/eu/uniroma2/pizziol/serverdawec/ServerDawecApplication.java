package eu.uniroma2.pizziol.serverdawec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication
public class ServerDawecApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerDawecApplication.class, args);

        // Create the table 'feedback' in the database (if it doesn't exist already)
        String createSQLiteCommand = "'CREATE TABLE IF NOT EXISTS feedback ( " +
                "product INTEGER PRIMARY KEY, " +
                "score NUMERIC NOT NULL DEFAULT 0, " +
                "preferred INTEGER NOT NULL DEFAULT 0, " +
                "textnote VARCHAR(255) NOT NULL DEFAULT 'None', " +
                "FOREIGN KEY(product) REFERENCES product(id));'";
        String createFeedback = "sqlite3 " + System.getProperty("user.dir") + "/s3scraper.db " + createSQLiteCommand;
        try {
            runCommand(createFeedback);
        } catch (IOException e) {
            System.out.println("Exception happened - here's what I know: ");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Execute a different process.
     * TODO: fix code duplication
     *
     * @param command The command used to run the process in the prompt.
     * @throws IOException Exception.
     */
    public static void runCommand(String command) throws IOException {
        String s;
        String[] cmd = {"/bin/sh", "-c", command};
        Process p = Runtime.getRuntime().exec(cmd);

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

        // Output from the command
        System.out.println("Here is the standard output of the command:\n");
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }

        // Errors from the attempted command
        System.out.println("Here is the standard error of the command (if any):\n");
        while ((s = stdError.readLine()) != null) {
            System.out.println(s);
        }

        // Kill the process
        p.destroy();
    }
}
