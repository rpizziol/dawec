package eu.uniroma2.pizziol.serverdawec.model.dto;

public class SearchMessageDto {

    private long id;
    private String inputText;
    private String targetSite;
    private int searchLimit;
    private int numberOfProxies;
    private boolean details;
    private boolean automaticMode;
    public String frequency;
    public String automaticTime;
    public int timeout;

    public SearchMessageDto(long id, String inputText, String targetSite, int searchLimit, int numberOfProxies,
                            boolean details, boolean automaticMode, String frequency, String automaticTime,
                            int timeout) {
        this.id = id;
        this.inputText = inputText;
        this.targetSite = targetSite;
        this.searchLimit = searchLimit;
        this.numberOfProxies = numberOfProxies;
        this.details = details;
        this.automaticMode = automaticMode;
        this.frequency = frequency;
        this.automaticTime = automaticTime;
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isAutomaticMode() {
        return automaticMode;
    }

    public void setAutomaticMode(boolean automaticMode) {
        this.automaticMode = automaticMode;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public String getTargetSite() {
        return targetSite;
    }

    public void setTargetSite(String targetSite) {
        this.targetSite = targetSite;
    }

    public int getSearchLimit() {
        return searchLimit;
    }

    public void setSearchLimit(int searchLimit) {
        this.searchLimit = searchLimit;
    }

    public int getNumberOfProxies() {
        return numberOfProxies;
    }

    public void setNumberOfProxies(int numberOfProxies) {
        this.numberOfProxies = numberOfProxies;
    }

    public boolean isDetails() {
        return details;
    }

    public void setDetails(boolean details) {
        this.details = details;
    }

    public String getAutomaticTime() {
        return automaticTime;
    }

    public void setAutomaticTime(String automaticTime) {
        this.automaticTime = automaticTime;
    }

}
