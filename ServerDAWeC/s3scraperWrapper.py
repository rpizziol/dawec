#!/usr/bin/python

import sys
import pexpect

child = pexpect.spawn('python3 /home/robb/Workspace/PyCharm/s3-scraper-master/s3scraper/scraper.py')

# Target site selection
child.expect(".*do:")
child.sendline(sys.argv[1])
print(child.after.decode('ascii'))

# Scraper tool selection
child.expect(".*do:")
child.sendline(sys.argv[2])
print(child.after.decode('ascii'))

# Details
child.expect(".*n]:")
child.sendline(sys.argv[3])
print(child.after.decode('ascii'))

# Search limit
child.expect(r".*p\):")
child.sendline(sys.argv[4])
print(child.after.decode('ascii'))

# Number of of proxies
child.expect(".*10]:")
child.sendline(sys.argv[5])
print(child.after.decode('ascii'))

# Search phrase
child.expect(".*phrase:")
child.sendline(' '.join(sys.argv[6:]))  # Concatenate all remaining args
print(child.after.decode('ascii'))


while True:
    child.expect(".*\r\n", timeout=600)  # 10 minutes timeout for each line
    print(child.after.decode('ascii'))

# child.wait()
# print(child.before.decode('ascii'))
# child.close()
